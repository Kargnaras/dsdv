package backend;

import model.ExecutionTrace;
import model.Frame;
import model.HeapEntity;
import model.HeapObject;
import model.HeapPrimitive;
import com.sun.jdi.*;
import java.util.*;
import static backend.TracerUtils.displayNameForType;
import static backend.TracerUtils.doesImplementInterface;
import static backend.TracerUtils.getIterator;
import static backend.TracerUtils.invokeSimple;

public class Tracer {

	private static final String[] INTERNAL_PACKAGES = {"java.", "javax.", "sun.", "jdk.", "com.sun.", "com.intellij.", "com.aed.java_visualizer.", "org.junit.", "jh61b.junit.", "jh61b.",};
	private static final List<String> BOXED_TYPES = Arrays.asList("Byte", "Short", "Integer", "Long", "Float", "Double", "Character", "Boolean");
	private TreeMap<Long, ObjectReference> pendingConversion = new TreeMap<>();
	private static final boolean SHOW_ALL_FIELDS = false;
	private ThreadReference thread;
	private ExecutionTrace model;

	public Tracer(ThreadReference thread) {

		this.thread = thread;
	}

	public ExecutionTrace getModel() throws Exception {

		model = new ExecutionTrace();
		Set<Long> heapDone = new HashSet<>();

		for (StackFrame frame : thread.frames()) {
			if (shouldShowFrame(frame)) {
				model.frames.add(convertFrame(frame));
			}
		}

		while (!pendingConversion.isEmpty()) {

			long id = pendingConversion.firstEntry().getKey();
			ObjectReference obj = pendingConversion.firstEntry().getValue();

			pendingConversion.remove(id);

			if (!heapDone.contains(id)) {

				heapDone.add(id);
				HeapEntity converted = convertObject(obj);
				converted.id = id;
				model.heap.put(id, converted);
			}
		}

		return model;
	}

	private static boolean shouldShowFrame(StackFrame frame) {

		Location loc = frame.location();
		return !isInternalPackage(loc.toString()) && !loc.method().name().contains("$access");
	}

	private Frame convertFrame(StackFrame sf) throws Exception {

		if (!thread.isSuspended()) {
			throw new IllegalThreadStateException();
		}

		Frame frame = new Frame();
		frame.name = sf.location().method().name() + ":" + sf.location().lineNumber();

		if (sf.thisObject() != null) {
			frame.locals.put("this", convertValue(sf.thisObject()));
		}

		Value value;
		List<LocalVariable> frame_args = sf.location().method().arguments();

		for (LocalVariable lv : frame_args) {

			if (lv.name().equals("args")) {

				value = sf.getValue(lv);

				if (value instanceof ArrayReference && ((ArrayReference) value).length() == 0) { continue; }
			}

			frame.locals.put(lv.name(), convertValue(sf.getValue(lv)));
		}

		if (frame_args.size() != sf.getArgumentValues().size()) {

			List<Value> anon_args = sf.getArgumentValues();

			for (int i = 0; i < anon_args.size(); i++) {

				String name = "param#" + i;
				frame.locals.put(name, convertValue(anon_args.get(i)));
			}
		}

		TreeMap<Integer, LocalVariable> orderByHash =  new TreeMap<>();
		List<LocalVariable> frame_vars = sf.location().method().variables();
		int offset = frame_vars.get(0).hashCode();

		for (LocalVariable lv : frame_vars) {

			if (!lv.isArgument() && (SHOW_ALL_FIELDS || !lv.name().endsWith("$"))) {

				orderByHash.put(lv.hashCode() - offset, lv);
			}
		}

		for (Map.Entry<Integer, LocalVariable> me : orderByHash.entrySet()) {

			if (me != null) {

				frame.locals.put(me.getValue().name(), convertValue(sf.getValue(me.getValue())));
			}
		}

		return frame;
	}

	private HeapPrimitive convertValue(Value value) {

		if (!(value instanceof ObjectReference)) {
			return new HeapPrimitive("null", HeapPrimitive.Type.NULL);
		}
		if (value instanceof VoidValue) {
			return new HeapPrimitive("void", HeapPrimitive.Type.VOID);
		}
		if (value instanceof BooleanValue) {
			return new HeapPrimitive(Boolean.toString(((BooleanValue) value).value()), HeapPrimitive.Type.BOOLEAN);
		}
		if (value instanceof CharValue) {
			return new HeapPrimitive(Character.toString(((CharValue) value).value()), HeapPrimitive.Type.CHAR);
		}
		if (value instanceof ByteValue) {
			return new HeapPrimitive(Byte.toString(((ByteValue) value).value()), HeapPrimitive.Type.BYTE);
		}
		if (value instanceof ShortValue) {
			return new HeapPrimitive(Short.toString(((ShortValue) value).value()), HeapPrimitive.Type.SHORT);
		}
		if (value instanceof IntegerValue) {
			return new HeapPrimitive(Integer.toString(((IntegerValue) value).value()), HeapPrimitive.Type.INT);
		}
		if (value instanceof LongValue) {
			return new HeapPrimitive(Long.toString(((LongValue) value).value()), HeapPrimitive.Type.LONG);
		}
		if (value instanceof FloatValue) {
			return new HeapPrimitive(Float.toString(((FloatValue) value).value()), HeapPrimitive.Type.FLOAT);
		}
		if (value instanceof DoubleValue) {
			return new HeapPrimitive(Double.toString(((DoubleValue) value).value()), HeapPrimitive.Type.DOUBLE);
		}
		if (value instanceof StringReference) {
			return new HeapPrimitive(((StringReference) value).value(), HeapPrimitive.Type.STRING);
		}

		return convertReference((ObjectReference) value);
	}

	private HeapPrimitive convertReference(ObjectReference obj) {

		if (obj.referenceType().name().startsWith("java.lang.") && BOXED_TYPES.contains(obj.referenceType().name().substring(10))) {

			return convertValue(obj.getValue(obj.referenceType().fieldByName("value")));
		}

		pendingConversion.put(obj.uniqueID(), obj);

		return new HeapPrimitive(Long.toString(obj.uniqueID()), HeapPrimitive.Type.REFERENCE);
	}





	private HeapEntity convertObject(ObjectReference obj) {

		if (obj instanceof ArrayReference) {
			ArrayReference ao = (ArrayReference) obj;
			int length = ao.length();

			HeapList out = new HeapList();
			out.type = HeapEntity.Type.LIST;
			out.label = ao.type().name();
			for (int i = 0; i < length; i++) {
				// TODO: optional feature, skip runs of zeros
				out.items.add(convertValue(ao.getValue(i)));
			}
			return out;

		}

		else if (obj instanceof StringReference) {
			HeapPrimitive out = new HeapPrimitive();
			out.type = HeapEntity.Type.PRIMITIVE;
			out.label = "String";
			out.value = new Value();
			out.value.type = Value.Type.STRING;
			out.value.stringValue = ((StringReference) obj).value();
			return out;
		}

		String typeName = obj.referenceType().name();
		if ((doesImplementInterface(obj, "java.util.List")
				|| doesImplementInterface(obj, "java.util.Set"))
				&& isInternalPackage(typeName)) {
			HeapList out = new HeapList();
			out.type = HeapEntity.Type.LIST; // XXX: or SET
			out.label = displayNameForType(obj);
			Iterator<com.sun.jdi.Value> i = getIterator(thread, obj);
			while (i.hasNext()) {
				out.items.add(convertValue(i.next()));
			}
			return out;
		}

		if (doesImplementInterface(obj, "java.util.Map") && isInternalPackage(typeName)) {
			HeapMap out = new HeapMap();
			out.type = HeapEntity.Type.MAP;
			out.label = displayNameForType(obj);

			ObjectReference entrySet = (ObjectReference) invokeSimple(thread, obj, "entrySet");
			Iterator<com.sun.jdi.Value> i = getIterator(thread, entrySet);
			while (i.hasNext()) {
				ObjectReference entry = (ObjectReference) i.next();
				HeapMap.Pair pair = new HeapMap.Pair();
				pair.key = convertValue(invokeSimple(thread, entry, "getKey"));
				pair.val = convertValue(invokeSimple(thread, entry, "getValue"));
				out.pairs.add(pair);
			}
			return out;
		}

		// now, arbitrary objects
		HeapObject out = new HeapObject();
		out.type = HeapEntity.Type.OBJECT;
		out.label = displayNameForType(obj);

		ReferenceType refType = obj.referenceType();

		if (shouldShowDetails(refType)) {
			// fields: -inherited -hidden +synthetic
			// visibleFields: +inherited -hidden +synthetic
			// allFields: +inherited +hidden +repeated_synthetic
			Map<Field, Value> fields = obj.getValues(SHOW_ALL_FIELDS ? refType.allFields() : refType.visibleFields());

			for (Map.Entry<Field, com.sun.jdi.Value> me : fields.entrySet()) {
				if (!me.getKey().isStatic() && (SHOW_ALL_FIELDS || !me.getKey().isSynthetic())) {
					String name = SHOW_ALL_FIELDS ? me.getKey().declaringType().name() + "." : "";
					name += me.getKey().name();
					Value value = convertValue(me.getValue());
					out.fields.put(name, value);
				}
			}
		}
		return out;
	}

	private static boolean isInternalPackage(final String name) {

		return Arrays.stream(INTERNAL_PACKAGES).anyMatch(name::startsWith);
	}

	private static boolean shouldShowDetails(ReferenceType type) {

		return !isInternalPackage(type.name());
	}
}
