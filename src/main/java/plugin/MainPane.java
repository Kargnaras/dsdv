package plugin;

import model.ExecutionTrace;
import com.intellij.ide.util.PropertiesComponent;
import com.intellij.ui.components.JBScrollPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import java.awt.BorderLayout;

class MainPane extends JPanel {

    ExecutionTrace executionTrace;

	public MainPane() {

	    executionTrace = null;
	}

	void setTrace(ExecutionTrace trace) {

		executionTrace = trace;
	}
}
