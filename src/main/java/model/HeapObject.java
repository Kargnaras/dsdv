package model;

import java.util.Map;
import java.util.TreeMap;

public class HeapObject extends HeapEntity {

	public Map<String, HeapPrimitive> fields = new TreeMap<>();

	@Override
	public boolean hasSameStructure(HeapEntity other) {

		return other.type == Type.OBJECT;
	}

}
