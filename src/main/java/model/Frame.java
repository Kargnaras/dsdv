package model;

import java.util.Map;
import java.util.TreeMap;

public class Frame {

	public String name;
	public Map<String, HeapPrimitive> locals = new TreeMap<>();
}
