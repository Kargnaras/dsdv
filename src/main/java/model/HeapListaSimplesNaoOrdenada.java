package model;

public class HeapListaSimplesNaoOrdenada extends HeapEntity {

    @Override
    public boolean hasSameStructure(HeapEntity other) {
        if (other instanceof HeapListaSimplesNaoOrdenada) {
            return true;
        }
        return false;
    }
}
