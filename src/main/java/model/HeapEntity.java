package model;

public abstract class HeapEntity {

	public long id;
	public Type type;
	public String label;

	public enum Type {

		PRIMITIVE, OBJECT, LISTA_DUPLA, LISTA_SIMPLES_NAO_ORDENADA, NO_LISTA_SIMPLES
	}

	public abstract boolean hasSameStructure(HeapEntity other);

}
