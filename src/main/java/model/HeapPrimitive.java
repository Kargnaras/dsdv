package model;

public class HeapPrimitive extends HeapEntity {

	public enum Type {

		NULL, VOID, BOOLEAN, CHAR, BYTE, SHORT, INT, LONG, FLOAT, DOUBLE, STRING, REFERENCE
	}

	private Type type;
	private String stringValue;

	public HeapPrimitive(String stringValue, Type type) {

		this.stringValue = stringValue;
		this.type = type;
	}

	@Override
	public boolean hasSameStructure(HeapEntity other) {

		return other.type == HeapEntity.Type.PRIMITIVE;
	}

	public Type getType() {

		return type;
	}

	public String getStringValue() {

		return stringValue;
	}
}
